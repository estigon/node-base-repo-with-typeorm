import { HttpStatus } from "../shared/HttpStatusEnum";

export class NotFoundError extends Error {
  status: number;
  message: string;

  constructor(message?: string) {
    super(message || 'Not Found');
    this.name = 'NotFoundError';
    this.status = HttpStatus.NOT_FOUND;
    this.message = message || 'Not Found';
  }
}