import express, { Application, Request, Response } from "express";
import "express-async-errors";
import cors from "cors";
import { AppDataSource } from "../data-source";
import userRoutes from "../routes/user";
import addressRoutes from "../routes/address";
import { errorHandler } from "../middlewares/errorHandler";

export class Server {

    private app: Application;
    private port: string;
    private apiPaths = {
        user: "/api/v1/users",
        address: "/api/v1/address",
    }

    constructor(){
        this.app = express();
        this.port = process.env.PORT || '3100';

        this.startDatabase();

        // add middlewares
        this.addMiddlewares();
        
        // add routes
        this.routes();
    }

    private addMiddlewares() {
        // cors
        this.app.use(cors())

        // read body on post
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: true}));
    }

    async startDatabase(){
        try {
            await AppDataSource.initialize();
            console.log('database connected!!!')
        } catch (error) {
            throw new Error(`error to connect database: ${error}`);
        }
    }

    routes(){
        this.app.get("/", (req: Request, res: Response) => res.json({ message: "The API is working!!!"}));
        this.app.use(this.apiPaths.user, userRoutes);
        this.app.use(this.apiPaths.address, addressRoutes);
        // global error handler
        this.app.use('*', errorHandler);
    }

    init(){
        this.app.listen(this.port, () => {
            console.log(`Server started on port ${this.port}`);
        })
    }

}