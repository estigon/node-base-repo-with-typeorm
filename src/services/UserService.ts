import { Repository } from 'typeorm';
import { CreateDTO } from '../dtos/user/createDTO';
import { UpdateDTO } from '../dtos/user/updateDTO';
import { User } from '../entities/User';
import { GenericService } from './base/GenericService';

export class UserService extends GenericService<User, CreateDTO, UpdateDTO> {
  protected userRepository: Repository<User>
  constructor(userRepository: Repository<User>) {
    super(userRepository);
    this.userRepository = userRepository;
  }

  async findByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({ where: { email } });
  }
  
  async deleteByEmail(email: string): Promise<boolean> {
    const result = await this.userRepository.delete({ email });
    return result.affected === 1;
  }
}
