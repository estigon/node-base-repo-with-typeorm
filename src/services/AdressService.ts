import { Repository } from 'typeorm';
import { CreateDTO } from '../dtos/address/createDTO';
import { UpdateDTO } from '../dtos/address/updateDTO';
import { Address } from '../entities/Address';
import { GenericService } from './base/GenericService';

export class AddressService extends GenericService<Address, CreateDTO, UpdateDTO> {
  protected addressRepository: Repository<Address>
  constructor(addressRepository: Repository<Address>) {
    super(addressRepository);
    this.addressRepository = addressRepository;
  }

}
