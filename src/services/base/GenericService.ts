import { Repository, FindOneOptions, FindOptionsWhere, Equal } from 'typeorm';
import { NotFoundError } from '../../config/errors/NotFoundError';
import { IBaseService } from './IBaseService';

export abstract class GenericService<T, C, U> implements IBaseService<T, C, U> {
    protected repository: Repository<T>;

    constructor(repository: Repository<T>) {
        this.repository = repository;
    }

    getAll(): Promise<T[]> {
       return this.repository.find();
    }

    async getOneById(id: number): Promise<T> {
        const idParam = id as FindOneOptions<T>
        const data = await this.repository.findOneBy({ id } as unknown as FindOptionsWhere<T>); // it doesn't recognize id as a property of T
        if(data){
            return data;
        }
        throw new NotFoundError(`Not Found`);
    }

    createOne(model: C): Promise<T> {
        throw new Error('Method not implemented.');
    }
    update(id: number, model: U): Promise<T> {
        throw new Error('Method not implemented.');
    }
    delete(id: number): Promise<boolean> {
        throw new Error('Method not implemented.');
    }


//   async create(data: DeepPartial<T>): Promise<T> {
//     const entity = this.repository.create(data);
//     return this.repository.save(entity);
//   }

    // async create(data: C): Promise<T> {
    //     const entity = this.createFromDto(data);
    //     return this.repository.save(entity);
    // }

    // async update(id: number, data: DeepPartial<T>): Promise<T | undefined> {
    //     // @ts-ignore
    //     const entity = await this.repository.findOneBy(id);
    //     if (!entity) {
    //     return undefined;
    //     }
    //     this.repository.merge(entity, data);
    //     return this.repository.save(entity);
    // }

    // async delete(id: number): Promise<boolean> {
    //     const result = await this.repository.delete(id);
    //     return result.affected === 1;
    // }

    private createFromDto(dto: C): T {
        // const entity = new this.repository.target();
        const entity = this.repository.create();
        Object.assign(entity, dto);
        return entity;
    }

    private updateFromDto(entity: T, dto: U): T {
        Object.assign(entity, dto);
        return entity;
    }
}
