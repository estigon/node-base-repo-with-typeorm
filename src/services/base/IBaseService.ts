import { FindOptionsWhere } from "typeorm";

// Model T, ID, K
export interface IBaseService<T, C, U> {
    getAll(): Promise<Array<T>>
    createOne(model: C): Promise<T>;
    getOneById(id: number): Promise<T>;
    update(id: number, model: U): Promise<T>;
    delete(id: number): Promise<boolean>;
}
