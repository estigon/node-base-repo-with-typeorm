import * as dotenv from 'dotenv';
import "reflect-metadata"
dotenv.config();
import { Server } from "./server/server";

const server = new Server();

server.init();