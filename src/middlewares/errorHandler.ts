import { Request, Response, NextFunction } from "express";
import { NotFoundError } from "../config/errors/NotFoundError";
import { HttpResponse } from "../config/responses/HttpResponse";

const httpResponse: HttpResponse = new HttpResponse();
export const errorHandler = (
    error: any,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if(error instanceof NotFoundError){
        return httpResponse.NotFound(res, error.message);
    }

    // if error don't match with our custom errors
    return httpResponse.Error(res, 'Unknown error');
}
