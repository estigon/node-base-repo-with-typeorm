import { DataSource } from "typeorm";
import { Address } from "./entities/Address";
import { User } from "./entities/User";


export const AppDataSource = new DataSource({
    type: "mysql",
    host: process.env.DATABASE_HOST,
    port: Number.parseInt(process.env.DATABASE_PORT, 10),
    username: process.env.USER_NAME,
    password: process.env.PASSWORD,
    database: process.env.DATABASE_NAME,
    synchronize: true,
    logging: true,
    entities: [User, Address],
    
    // subscribers: [],
    // migrations: [],
    
});
