import { BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm';

export default abstract class GenericEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;
}
