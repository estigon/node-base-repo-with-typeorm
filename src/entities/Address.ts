import { PrimaryGeneratedColumn, Column, Entity, BaseEntity, DeleteDateColumn, CreateDateColumn } from "typeorm";

@Entity()
export class Address extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;
}