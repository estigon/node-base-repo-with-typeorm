import { PrimaryGeneratedColumn, Column, Entity, BaseEntity, DeleteDateColumn, CreateDateColumn } from "typeorm";

@Entity()
export class User extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({name: 'last_name'})
    lastName: string;

    @Column({ length: 20 })
    dni: string;

    @Column({ length: 30 })
    email: string;

    @CreateDateColumn()
    createdAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}