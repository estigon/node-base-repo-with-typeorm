import { Router } from "express";
import { AddressController } from "../controllers/AddressController";

const addressController = new AddressController();
const router = Router();

router.get('/', addressController.findAll.bind(addressController));
router.get('/:id', (req, res) => addressController.findOne(req, res));
// router.get('/:id', userControllerImpl.getOne);
// router.post('/', createOne);
// router.put('/:id', updateOne);
// router.delete('/:id', deleteOne);

export default router;