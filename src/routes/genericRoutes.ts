// import { Router } from 'express';
// import GenericController from '../controllers/GenericController';
// import GenericDTO from '../dtos/GenericDTO';
// import GenericService from '../services/GenericService';

// export default function genericRoutes<T>(
//   path: string,
//   service: GenericService<T, GenericDTO>,
// ): Router {
//   const router = Router();
//   const controller = new GenericController(service);

//   router.get('/', controller.findAll.bind(controller));
//   router.get('/:id', controller.findOne.bind(controller));
//   router.post('/', controller.create.bind(controller));
//   router.put('/:id', controller.update.bind(controller));
//   router.delete('/:id', controller.delete.bind(controller));

//   return router;
// }
