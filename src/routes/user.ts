import { Router } from "express";
// import {
//     getAllWithoutPagination, 
//     createOne, 
//     getOne, 
//     updateOne, 
//     deleteOne, 
// } from "../controllers/UserController";

import { AppDataSource } from "../data-source";
import { User } from "../entities/User";
import {UserService } from "../services/UserService";
import { UserController } from "../controllers/UserController";
// const userService = new UserService(AppDataSource.getRepository(User));
const userController = new UserController();

const router = Router();

router.get('/', userController.findAll.bind(userController));
router.get('/:id', (req, res) => userController.findOne(req, res));
// router.get('/:id', userControllerImpl.getOne);
// router.post('/', createOne);
// router.put('/:id', updateOne);
// router.delete('/:id', deleteOne);

export default router;