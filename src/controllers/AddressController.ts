import { AppDataSource } from "../data-source";
import { CreateDTO } from '../dtos/address/createDTO';
import { UpdateDTO } from '../dtos/address/updateDTO';
import {Address} from '../entities/Address';
import { AddressService } from '../services/AdressService';
import { GenericController } from './base/GenericController';

export class AddressController extends GenericController<Address, CreateDTO, UpdateDTO>  {
  private addressService: AddressService;
  constructor() {
    const addressRepository = new AddressService(AppDataSource.getRepository(Address));
    super(addressRepository);
    this.addressService = new AddressService(AppDataSource.getRepository(Address));
  }

}
