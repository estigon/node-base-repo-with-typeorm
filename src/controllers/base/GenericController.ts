import { Request, Response } from 'express';
import { HttpResponse } from '../../config/responses/HttpResponse';
import { IBaseService } from '../../services/base/IBaseService';

export abstract class GenericController<T, C, U> {
  
  private service: IBaseService<T, C, U>;
  protected httpResponse: HttpResponse;
    
  constructor(service: any) {
    this.service = service;
    this.httpResponse = new HttpResponse();
  }

  async findAll(req: Request, res: Response): Promise<void> {
    const dtos = await this.service.getAll();
    this.httpResponse.Ok(res, dtos);
  }

  async findOne(req: Request, res: Response): Promise<void> {
    const id = parseInt(req.params.id);
    const dto = await this.service.getOneById(id);
    this.httpResponse.Ok(res, dto);
  }

  async create(req: Request, res: Response): Promise<void> {
    // const dto = req.body as C;
    // const createdDto = await this.service.create(dto);
    // res.status(201).json(createdDto);
  }

//   async update(req: Request, res: Response): Promise<void> {
//     const id = parseInt(req.params.id);
//     const dto = req.body as U;
//     const updatedDto = await this.service.update(id, dto);
//     if (updatedDto) {
//       res.json(updatedDto);
//     } else {
//       res.status(404).send();
//     }
//   }

  async delete(req: Request, res: Response): Promise<void> {
    const id = parseInt(req.params.id);
    const result = await this.service.delete(id);
    if (result) {
      res.status(204).send();
    } else {
      res.status(404).send();
    }
  }
}
