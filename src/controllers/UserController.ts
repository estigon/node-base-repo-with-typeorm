import { Request, Response } from 'express';
import { AppDataSource } from "../data-source";
import { CreateDTO } from '../dtos/user/createDTO';
import { UpdateDTO } from '../dtos/user/updateDTO';
import {User} from '../entities/User';
import {UserService} from '../services/UserService';
// import UserDTO from '../dtos/UserDTO';
import { GenericController } from './base/GenericController';

export class UserController extends GenericController<User, CreateDTO, UpdateDTO>  {
  private userService: UserService;
  constructor() {
    const userRepository = new UserService(AppDataSource.getRepository(User));
    super(userRepository);
    this.userService = new UserService(AppDataSource.getRepository(User));
    // this.service = new UserService(AppDataSource.getRepository(User))
    // super(userService);
  }

  async findByEmail(req: Request, res: Response) {
    const email = req.params.email;
    const user = await this.userService.findByEmail(email);
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
    return res.json(user);
  }

  async deleteByEmail(req: Request, res: Response) {
    const email = req.params.email;
    const result = await this.userService.deleteByEmail(email);
    if (!result) {
      return res.status(404).json({ message: 'User not found' });
    }
    return res.json({ message: 'User deleted successfully' });
  }

}
