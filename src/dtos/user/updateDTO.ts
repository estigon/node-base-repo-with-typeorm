export class UpdateDTO {
    name?: string;
    lastName?: string;
    dni?: string;
    email?: string;
    // createdAt: Date;
    // deletedAt: Date;
}