export interface GenericDTO {
  id?: number;
  createdAt?: Date;
  updatedAt?: Date;
}
  